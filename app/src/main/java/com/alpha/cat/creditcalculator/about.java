package com.alpha.cat.creditcalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import static com.alpha.cat.creditcalculator.MainActivity.locale;
import static com.alpha.cat.creditcalculator.MainActivity.setWindowFlag;
import static com.alpha.cat.creditcalculator.StringVariants.descriptionRU;
import static com.alpha.cat.creditcalculator.StringVariants.descriptionUA;

public class about extends AppCompatActivity {
    ImageView back;
    TextView aboutMainText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        back = findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        aboutMainText = findViewById(R.id.aboutMainText);
        aboutMainText.setText(descriptionRU);
        if (locale.equals("uk"))
            aboutMainText.setText(descriptionUA);
    }


}
