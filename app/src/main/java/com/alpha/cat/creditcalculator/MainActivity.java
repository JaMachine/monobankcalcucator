package com.alpha.cat.creditcalculator;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;

import java.text.DecimalFormat;
import java.util.Locale;

import static com.alpha.cat.creditcalculator.StringVariants.generateUA;
import static com.alpha.cat.creditcalculator.StringVariants.loanUA;
import static com.alpha.cat.creditcalculator.StringVariants.minimumUA;
import static com.alpha.cat.creditcalculator.StringVariants.monthlyPaymentUA;
import static com.alpha.cat.creditcalculator.StringVariants.okUA;
import static com.alpha.cat.creditcalculator.StringVariants.payOffUA;
import static com.alpha.cat.creditcalculator.StringVariants.setMonthCount;

public class MainActivity extends AppCompatActivity {
    static int month = 3, ads = 1;
    static String locale = Locale.getDefault().getLanguage();
    boolean resultsDisplayed, keyboard;
    EditText editText;
    static TextView number, getCardText, rateText, sum1Logo, sum1Int, sum2Logo, sum2Int, paymentLogo, paymentInt, overpriceLogo, overpriceInt, calculate_logo, setM, aboutText;
    ImageView plus, minus, info;
    CardView resultList, menu, aboutButton, getCardButton, underground, rateButton;
    RelativeLayout calculateResult;
    Context context;
    public static InterstitialAd mInterstitialAd;
    public static AdView mAdView;
    public static AdRequest adRequest;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initValues();
        transparentStatusBar();
        setUAlocal();
        ads();
    }

    @Override
    protected void onResume() {
        super.onResume();
        transparentStatusBar();
        setUAlocal();
    }

    private void changeMonth(boolean add) {
        if (add) {
            if (month < 24)
                month++;
        }
        if (!add) {
            if (month > 3)
                month--;
        }
        if (month == 3)
            minus.setImageResource(R.drawable.minus_dis);
        if (month == 24)
            plus.setImageResource(R.drawable.plus_dis);
        if (month > 3 && month < 24) {
            plus.setImageResource(R.drawable.plus_ena);
            minus.setImageResource(R.drawable.minus_ena);
        }
        number.setText("" + month);
        editText.setFocusable(false);
        setMonthCount();
    }

    @SuppressLint("ClickableViewAccessibility")
    private void initValues() {
        number = findViewById(R.id.number);

        underground = findViewById(R.id.underground);

        aboutText = findViewById(R.id.aboutText);
        rateText = findViewById(R.id.RateText);
        getCardText = findViewById(R.id.getCardText);

        rateButton = findViewById(R.id.RateButton);
        rateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.alpha.cat.creditcalculator")));
            }
        });

        aboutButton = findViewById(R.id.aboutButton);
        aboutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
                startActivity(new Intent(".about"));
            }
        });

        getCardButton = findViewById(R.id.getCardButton);
        getCardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
                context.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://monobank.ua/r/fT1M")));
            }
        });

        info = findViewById(R.id.info);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMenu();
                hideKeyboard();
            }
        });

        menu = findViewById(R.id.menu);

        plus = findViewById(R.id.plus);
        plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMonth(true);
                hideKeyboard();
            }
        });

        minus = findViewById(R.id.minus);
        minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                changeMonth(false);
                hideKeyboard();
            }
        });

        editText = findViewById(R.id.editText);
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });
        editText.setFocusable(false);
        editText.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                editText.setFocusableInTouchMode(true);
                keyboard = true;
                return false;
            }
        });

        sum1Logo = findViewById(R.id.sum1_logo);
        sum1Int = findViewById(R.id.sum1_int);
        sum2Logo = findViewById(R.id.sum2_logo);
        sum2Int = findViewById(R.id.sum2_int);
        overpriceLogo = findViewById(R.id.overprice_logo);
        overpriceInt = findViewById(R.id.overprice_int);
        paymentLogo = findViewById(R.id.payment_logo);
        paymentInt = findViewById(R.id.payment_int);
        resultList = findViewById(R.id.result);
        setM = findViewById(R.id.setM);

        calculate_logo = findViewById(R.id.calculate_logo);

        calculateResult = findViewById(R.id.calculate_result);
        calculateResult.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard();
                String message = "Минимальная сумма 1000 грн.";
                if (locale.equals("uk"))
                    message = minimumUA;

                if (editText.getText().toString().equals("")) {
                    editText.setText("1000");
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } else if (Double.parseDouble(String.valueOf(editText.getText())) < 1000.0) {
                    editText.setText("1000");
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } else
                    resultsHandler();
            }
        });

        context = this;

    }

    private void resultsHandler() {
        editText.setFocusable(false);
        if (!resultsDisplayed) {
            resultsDisplayed = true;
            resultList.setVisibility(View.VISIBLE);
            underground.setVisibility(View.VISIBLE);
            info.setVisibility(View.GONE);
            editText.setVisibility(View.GONE);

            calculate_logo.setText("Понятно");
            if (locale.equals("uk"))
                calculate_logo.setText(okUA);
            calculations();
        } else {
            showAds();
            resultsDisplayed = false;
            resultList.setVisibility(View.GONE);
            menu.setVisibility(View.GONE);
            underground.setVisibility(View.GONE);
            editText.setVisibility(View.VISIBLE);
            info.setVisibility(View.VISIBLE);
            calculate_logo.setText("рассчитать");
            if (locale.equals("uk"))
                calculate_logo.setText(generateUA);
        }
    }

    private void showMenu() {
        editText.setFocusable(false);
        if (!resultsDisplayed) {
            resultsDisplayed = true;
            menu.setVisibility(View.VISIBLE);
            underground.setVisibility(View.VISIBLE);
            info.setVisibility(View.GONE);
            editText.setVisibility(View.GONE);
            editText.setText("1000");
            calculate_logo.setText("Назад");
            if (locale.equals("uk"))
                calculate_logo.setText(okUA);
            calculations();
        } else {
            showAds();
            resultsDisplayed = false;
            menu.setVisibility(View.GONE);
            underground.setVisibility(View.GONE);
            resultList.setVisibility(View.GONE);
            editText.setVisibility(View.VISIBLE);
            info.setVisibility(View.VISIBLE);
            calculate_logo.setText("рассчитать");
            if (locale.equals("uk"))
                calculate_logo.setText(generateUA);
        }
    }

    private void calculations() {
        DecimalFormat df = new DecimalFormat("####0.00");

        double amount = Double.parseDouble(String.valueOf(editText.getText()));
        double amount2 = ((amount + amount * 0.019 * month) / month) * month;
        double a3 = amount2 - amount;
        sum1Int.setText("   " + df.format(amount) + " ₴");
        paymentInt.setText("   " + df.format((amount + amount * 0.019 * month) / month) + " ₴");
        sum2Int.setText("   " + df.format(amount2) + " ₴");
        overpriceInt.setText("   " + df.format(a3) + " ₴");
    }

    private void setUAlocal() {
        if (locale.equals("uk")) {
            sum1Logo.setText(loanUA);
            sum2Logo.setText(payOffUA);
            paymentLogo.setText(monthlyPaymentUA);
            calculate_logo.setText(generateUA);
            aboutText.setText("Детальніше про розстрочку");
            rateText.setText("Залишити відгук");
            getCardText.setText("Скачати додаток Monobank");
        }
        setMonthCount();
    }

    private void transparentStatusBar() {
        if (Build.VERSION.SDK_INT >= 19 && Build.VERSION.SDK_INT < 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, true);
        }
        if (Build.VERSION.SDK_INT >= 19) {
            getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        }
        //make fully Android Transparent Status bar
        if (Build.VERSION.SDK_INT >= 21) {
            setWindowFlag(this, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, false);
            getWindow().setStatusBarColor(Color.TRANSPARENT);
        }
    }

    public static void setWindowFlag(Activity activity, final int bits, boolean on) {
        Window win = activity.getWindow();
        WindowManager.LayoutParams winParams = win.getAttributes();
        if (on) {
            winParams.flags |= bits;
        } else {
            winParams.flags &= ~bits;
        }
        win.setAttributes(winParams);
    }

    private void hideKeyboard() {
        if (keyboard) {
            keyboard = false;
            editText.setFocusable(false);
            InputMethodManager imm = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
            imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
        }
    }

    public void ads() {
        MobileAds.initialize(this, new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-7809613831104914/2466089812");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        mAdView = findViewById(R.id.adView);
        adRequest = new AdRequest.Builder().build();
    }

    public static void showAds() {
        if (ads >= 3) {
            ads = 0;
            if (mInterstitialAd.isLoaded()) {
                mInterstitialAd.show();
                mInterstitialAd.loadAd(new AdRequest.Builder().build());
            }
        } else {
            ads++;
        }
        mAdView.loadAd(adRequest);
    }
}
