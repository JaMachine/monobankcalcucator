package com.alpha.cat.creditcalculator;


import static com.alpha.cat.creditcalculator.MainActivity.locale;
import static com.alpha.cat.creditcalculator.MainActivity.month;
import static com.alpha.cat.creditcalculator.MainActivity.setM;

public class StringVariants {

    public static String
            generateUA = "Розрахувати",
            okUA = "зрозуміло",
            m1UA = "місяць",
            m2UA = "місяці",
            m3UA = "місяців",
            loanUA = "Сума позики",
            monthlyPaymentUA = "щомісячний платіж",
            payOffUA = "сума погашення",
            minimumUA = "Мінімальна сума 1000 грн.",
            descriptionRU = "Приложение рассчитывает стоимость рассрочки (кредита) для клиентов Monobank.\n\n" +
                    "Любую трату из выписки превышающую 1000 грн. (перевод на карту, снятие наличных, счет в ресторане и т.д.) можно перевести в рассрочку в течение семи дней. В таком случае сумма покупки в полном размере будет возвращена на карту и разделена на выбранное количество месяцев. Первый платеж будет списан через месяц после оформления рассрочки.\n" +
                    "Комиссия составить 1.9% от суммы покупки в месяц.\n\n" +
                    "В случае досрочного погашения, комиссия оплачивается за весь период в полном размере.",
            descriptionUA = "Цей додаток розраховує вартість розстрочки (кредиту) для клієнтів Monobank. Будь-яку витрату з виписки що перевищує 1000 грн. (переказ на карту, зняття готівки, рахунок в ресторані і т.п.) можна перевести у розстрочку протягом семи днів. В такому випадку сума покупки в повному розмірі буде повернена на карту і розділена на вибрану кількість місяців. Перший платіж буде списаний через місяць після оформлення розстрочки.\n" +
                    "\n" +
                    "Комісія складатиме 1.9% від суми покупки на місяць.\n" +
                    "\n" +
                    "У разі дострокового погашення, комісія оплачується за весь період в повному обсязі.";

    public static void setMonthCount() {
        if (month == 3) {
            setM.setText("месяца");
            if (locale.equals("uk"))
                setM.setText(m2UA);
        }
        if (month == 4) {
            setM.setText("месяца");
            if (locale.equals("uk"))
                setM.setText(m2UA);
        }
        if (month >= 5 && month <= 20) {
            setM.setText("месяцев");
            if (locale.equals("uk"))
                setM.setText(m3UA);
        }
        if (month == 21) {
            setM.setText("месяц");
            if (locale.equals("uk"))
                setM.setText(m1UA);
        }
        if (month == 22) {
            setM.setText("месяца");
            if (locale.equals("uk"))
                setM.setText(m2UA);
        }
        if (month == 23) {
            setM.setText("месяца");
            if (locale.equals("uk"))
                setM.setText(m2UA);
        }
        if (month == 24) {
            setM.setText("месяца");
            if (locale.equals("uk"))
                setM.setText(m2UA);
        }
    }

}
